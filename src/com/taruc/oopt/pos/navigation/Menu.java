package com.taruc.oopt.pos.navigation;

public class Menu {
	private int index;
	private String name;
	private boolean isAction = false;
	private int actionId;
	private Menu[] subMenu;
	private int accessLevel = 10;
	
	public Menu(int index, String name) {
		super();
		this.index = index;
		this.name = name;		
	}
	public Menu(int index, String name, int accessLevel) {
		super();
		this.index = index;
		this.name = name;		
		this.accessLevel = accessLevel;
	}	
	public Menu(int index, String name, boolean isAction, int actionid) {
		super();
		this.index = index;
		this.name = name;		
		this.isAction = isAction;
		this.actionId = actionid;
	}
	public Menu(int index, String name, Menu[] subMenu) {
		super();
		this.index = index;
		this.name = name;
		this.subMenu = subMenu;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Menu[] getSubMenu() {
		return subMenu;
	}
	public void setSubMenu(Menu[] subMenu) {
		this.subMenu = subMenu;
	}
	public boolean isAction() {
		return isAction;
	}
	public void setAction(boolean isAction) {
		this.isAction = isAction;
	}
	public int getActionId() {
		return actionId;
	}
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
	public int getAccessLevel() {
		return accessLevel;
	}
	public void setAccessLevel(int accessLevel) {
		this.accessLevel = accessLevel;
	}
	
	
}
