package com.taruc.oopt.pos.hr;

public class FullTimer extends Employee {
	private double basicSalary;
	private double monthlyAllowance;
	public double getBasicSalary() {
		return basicSalary;
	}
	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}
	public double getMonthlyAllowance() {
		return monthlyAllowance;
	}
	public void setMonthlyAllowance(double monthlyAllowance) {
		this.monthlyAllowance = monthlyAllowance;
	}
}
