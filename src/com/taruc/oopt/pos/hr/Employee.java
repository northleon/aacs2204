package com.taruc.oopt.pos.hr;

import java.util.Date;

import com.taruc.oopt.pos.template.ListingInterface;

public class Employee implements ListingInterface{
	// personal info
	private String name;
	private String nric;
	private Date dob;
	private String gender;
	private String race;	
	
	// contact info
	private String email;
	private String mobileTel;
	private String address;
	
	// employee info
	private Date joinDate;	
	private boolean active;
	
	private String password;
	
	// Other info
	private Department department;
	private int accessLevel = 10;	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNric() {
		return nric;
	}

	public void setNric(String nric) {
		this.nric = nric;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileTel() {
		return mobileTel;
	}

	public void setMobileTel(String mobileTel) {
		this.mobileTel = mobileTel;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
	
	public int getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(int accessLevel) {
		this.accessLevel = accessLevel;
	}

	@Override
	public void toListingDisplay() {		
		System.out.printf("%20s %20s %30s %20s", name, nric, joinDate, department.name);
		System.out.println();
	}

	@Override
	public void toListingHeader() {		
		System.out.printf("%20s %20s %30s %20s", "Name", "NRIC", "DATE_JOINED", "DEPARTMENT");
		System.out.println();
		System.out.println("______________________________________________________________________________________________________");
	}	
	
	
}
