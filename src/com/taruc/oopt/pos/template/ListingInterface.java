package com.taruc.oopt.pos.template;

public interface ListingInterface {
	public void toListingHeader();
	public void toListingDisplay();
}
