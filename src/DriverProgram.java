import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import com.taruc.oopt.pos.hr.Department;
import com.taruc.oopt.pos.hr.Employee;
import com.taruc.oopt.pos.hr.FullTimer;
import com.taruc.oopt.pos.hr.Manager;
import com.taruc.oopt.pos.hr.PartTimer;
import com.taruc.oopt.pos.navigation.Menu;

public class DriverProgram {

	private static String defaultDateFormat = "yyyy-MM-dd";
	public static SimpleDateFormat dateFormatter = new SimpleDateFormat(defaultDateFormat);
	public static List<Employee> employeeList = new ArrayList<>();
	public static List<Menu> menuList = new ArrayList<>();
	
	public static Employee currentUser = null;

	public static void main(String[] args) {

		System.out.println("System initializing...");
		System.out.println();
		System.out.println("\tTARUC POS DEMO");
		try {
			loadInitialData();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	
		boolean validLogin = false; 
		while (!validLogin) {
			validLogin = promptLogin();
		}


		AtomicInteger userInput = new AtomicInteger(0);
		List<Menu> dynamicMenu = new ArrayList<>();
		dynamicMenu.addAll(menuList);
		
		do {
			Optional<Menu> menu = dynamicMenu.stream().filter(each -> each.getIndex() == userInput.get()).findFirst();
			if(userInput.get() == 0) {
				dynamicMenu.clear();
				dynamicMenu.addAll(menuList);
				
				userInput.set(generateMainMenu());				
			} else if (menu.isPresent()) {								
				if(menu.get().isAction()) {
					userInput.set(performTask(menu.get().getActionId()));					
				} else {
					if(menu.get().getSubMenu() != null) {
						dynamicMenu.clear();	
						dynamicMenu.addAll(Arrays.asList(menu.get().getSubMenu()));
					}
					
					userInput.set(generateMenu(menu.get()));
				}
					
			}			
			
		} while (userInput.get() != -1);
		
		shutdown();
	}
	
	public static int performTask(int taskId) {
		int userInput = 0;
		switch (taskId) {
		case 1001:
			userInput = viewListing(employeeList);
			break;

		}
		return userInput;
	}
	
	public static int viewListing(List<Employee> listing) {
		clearConsole();
		if(!listing.isEmpty()) {
			for(int i=0; i < listing.size(); i++) {
				if(i == 0) 
					listing.get(i).toListingHeader();				
				listing.get(i).toListingDisplay();
			}
		}
		
		return numericInput();
	}
	
	public static void shutdown() {
		clearConsole();
		System.out.print("Shuting down");
		for (int i=0; i< 3 ;i++) {
			try {
				TimeUnit.SECONDS.sleep(1);
				System.out.print(".");
			} catch (InterruptedException e) {}	
		}
		System.out.println("System Terminated.");
		System.exit(1);
	}

	public static void loadInitialData() throws ParseException {
		
		int menuCounter = 1;
		Menu[] employeeSub = new Menu[] { new Menu(menuCounter++, "Employee Listing",true, 1001), new Menu(menuCounter++, "Add New Employee",true, 1002)};		
		menuCounter = 1;
		Menu employeeMain = new Menu(menuCounter++,"Employee Panel", employeeSub);
		Menu productMain = new Menu(menuCounter++,"Product Panel");
		Menu salesMain = new Menu(menuCounter++,"Sales Panel");
		Menu reportMain = new Menu(menuCounter++,"Report Panel", 100);
				
		menuList.add(employeeMain);
		menuList.add(productMain);
		menuList.add(salesMain);
		menuList.add(reportMain);
		
		Department kitchen = new Department("Kitchen", "Kitchen");
		Department service = new Department("Service", "Service");
		Department management = new Department("Management", "Management");

		Manager manager_A = new Manager();
		manager_A.setName("Jason Toh");
		manager_A.setNric("12345678912");
		manager_A.setDob(dateFormatter.parse("1991-01-01"));
		manager_A.setGender("M");
		manager_A.setRace("CHINESE");
		manager_A.setEmail("jason@gmail.com");
		manager_A.setMobileTel("0123456789");
		manager_A.setAddress("No.1. Pesara King Edward, Georgetown, 10300 Penang, Malaysia");
		manager_A.setJoinDate(dateFormatter.parse("2015-01-01"));
		manager_A.setPassword("123123");
		manager_A.setAccessLevel(100);
		manager_A.setActive(true);
		manager_A.setDepartment(management);

		PartTimer partimer_A = new PartTimer();
		partimer_A.setName("William Lim");
		partimer_A.setNric("A41245123");
		partimer_A.setDob(dateFormatter.parse("1991-01-01"));
		partimer_A.setGender("M");
		partimer_A.setRace("CHINESE");
		partimer_A.setEmail("william@gmail.com");
		partimer_A.setMobileTel("0123456789");
		partimer_A.setAddress("No.1. Pesara King Edward, Georgetown, 10300 Penang, Malaysia");
		partimer_A.setJoinDate(dateFormatter.parse("2015-01-01"));
		partimer_A.setPassword("123123");
		partimer_A.setActive(true);
		partimer_A.setDepartment(service);
		
		FullTimer fulltimer_A = new FullTimer();
		fulltimer_A.setName("Ethan Wong");
		fulltimer_A.setNric("KA1341233");
		fulltimer_A.setDob(dateFormatter.parse("1991-01-01"));
		fulltimer_A.setGender("M");
		fulltimer_A.setRace("CHINESE");
		fulltimer_A.setEmail("ethan@gmail.com");
		fulltimer_A.setMobileTel("0123456789");
		fulltimer_A.setAddress("No.1. Pesara King Edward, Georgetown, 10300 Penang, Malaysia");
		fulltimer_A.setJoinDate(dateFormatter.parse("2015-01-01"));
		fulltimer_A.setPassword("123123");
		fulltimer_A.setActive(true);
		fulltimer_A.setDepartment(kitchen);
		
		employeeList.add(manager_A);
		employeeList.add(partimer_A);
		employeeList.add(fulltimer_A);
	}

	public static boolean promptLogin() {
		Scanner sc = new Scanner(System.in);

		System.out.print("Login : ");
		String username = sc.next();
		System.out.print("Password : ");
		String password = sc.next();

		// validate login
		Optional<Employee> loggedInUser = employeeList.stream().filter(eachEmployee -> eachEmployee.getEmail().equalsIgnoreCase(username))
				.findFirst().map(eachEmployee -> eachEmployee);
		
		boolean valid = loggedInUser.isPresent() && loggedInUser.get().getPassword().equalsIgnoreCase(password);

		if (!valid) {
			System.out.println("Failed to login. Please try again.");
		} else {
			currentUser = loggedInUser.get();
		}

		return valid;
	}

	public static int generateMainMenu() {
		return generateMenu(null);
	}
	
	public static int generateMenu(Menu menu) {
		clearConsole();
		Scanner sc = new Scanner(System.in);
		
		StringBuilder sb = new StringBuilder();
		final String breakline = "\n";
		final String delimiter = "\t";

		int userAccessLevel = currentUser.getAccessLevel();
		
		boolean isMain = menu == null;
		sb.append(breakline);
		sb.append("\t").append(isMain ?  "Main Menu" : menu.getName()).append(breakline);
		sb.append("___________________________________").append(breakline);
		
		Predicate<Menu> canAccess = eachItem -> userAccessLevel >= eachItem.getAccessLevel();
		
		if (menu != null) {
			if (menu.getSubMenu() != null) {
				Arrays.stream(menu.getSubMenu())
						.filter(canAccess)
						.forEach(eachItem -> sb.append(eachItem.getIndex()).append(delimiter).append(eachItem.getName()).append(breakline));
			}

		} else {
			menuList.stream()
					.filter(canAccess)
					.forEach(eachItem -> sb.append(eachItem.getIndex()).append(delimiter).append(eachItem.getName()).append(breakline));
		}
		
		sb.append(breakline);			

		System.out.print(sb.toString());
		
		int maxRange = isMain ? menuList.size() : menu.getSubMenu() != null ? menu.getSubMenu().length : 0;
		int choice = numericInput(0, maxRange, false);
	
		return choice;
	}		
	
	public final static void clearConsole() {
		for(int i = 0; i < 20; i++) {
			System.out.println("");
		}
	}
	
	public static int numericInput() {
		return numericInput(0,0,false);
	}
	public static int numericInput(int begin, int end, boolean hasError) {
		
		Scanner sc = new Scanner(System.in);
		int input = -1;
		
		if (hasError) {
			System.out.println("Invalid numeric value. Please try again.");						
			
		}
		System.out.println("");
		System.out.println("(-1) EXIT");
		System.out.println("( 0) MAIN MENU");
		System.out.print("Choose a number to continue: ");
		
		try {
			input = sc.nextInt();
			if (input != -1 && (input < begin || input > end))
				throw new Exception();
		}catch (Exception e) {
			input = numericInput(begin, end, true);
		}		
		
		return input;
	}
}
